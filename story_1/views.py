from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Ray Azrin Karim'
curr_year = int(datetime.now().strftime("%Y"))
# TODO Implement this, format (Year, Month, Date)
birth_date = date(2000, 4, 19)
npm = 1706044111  # TODO Implement this
university = 'Universitas Indonesia'
hobby = 'reading books, watching movies, and listening to music'
# Create your views here.


def index(request):
    response = {'name': mhs_name, 'age': calculate_age(
        birth_date.year), 'npm': npm, 'univ': university, 'hobby': hobby}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
