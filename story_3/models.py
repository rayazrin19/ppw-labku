from django.db import models
from datetime import datetime

# Create your models here.


class Schedule(models.Model):

    activity = models.CharField(max_length=30)
    day = models.CharField(max_length=10, null=True)
    date = models.DateTimeField(default=datetime.now)
    time = models.TimeField()
    place = models.CharField(max_length=20)
    category = models.CharField(max_length=20)

    def __str__(self):
        return self.activity
